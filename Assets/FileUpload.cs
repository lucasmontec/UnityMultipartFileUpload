﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using System;
using System.Text;

public class FileUpload : EditorWindow {
    private const string serverURL = "http://localhost:8080/";

    private string path;

    [MenuItem("Tools/File Upload")]
    public static void ShowWindow() {
        GetWindow(typeof(FileUpload));
    }

    private void OnGUI() {
        EditorGUILayout.LabelField(path);

        if (GUILayout.Button("upload") && !String.IsNullOrEmpty(path)) {
            Upload();
        }

        AcceptDrops();
    }

    private void Upload() {
        List<IMultipartFormSection> form = new List<IMultipartFormSection>{
            new MultipartFormFileSection("file", System.IO.File.ReadAllBytes(path), System.IO.Path.GetFileName(path), "file")
         };

        // generate a boundary then convert the form to byte[]
        byte[] boundary = UnityWebRequest.GenerateBoundary();
        byte[] formSections = UnityWebRequest.SerializeFormSections(form, boundary);
        // my termination string consisting of CRLF--{boundary}--
        byte[] terminate = Encoding.UTF8.GetBytes(String.Concat("\r\n--", Encoding.UTF8.GetString(boundary), "--"));
        // Make my complete body from the two byte arrays
        byte[] body = new byte[formSections.Length + terminate.Length];
        Buffer.BlockCopy(formSections, 0, body, 0, formSections.Length);
        Buffer.BlockCopy(terminate, 0, body, formSections.Length, terminate.Length);
        // Set the content type - NO QUOTES around the boundary
        string contentType = String.Concat("multipart/form-data; boundary=", Encoding.UTF8.GetString(boundary));
        // Make my request object and add the raw body. Set anything else you need here
        UnityWebRequest wr = new UnityWebRequest(serverURL);
        UploadHandler uploader = new UploadHandlerRaw(body);
        uploader.contentType = contentType;
        wr.uploadHandler = uploader;

        wr.method = "POST";
        wr.chunkedTransfer = false;
        wr.useHttpContinue = false;
        wr.SendWebRequest();
        Debug.Log("Sent!");
    }

    public void AcceptDrops() {
        Event evt = Event.current;

        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

        if (evt.type == EventType.DragPerform)
        {
            DragAndDrop.AcceptDrag();

            foreach (UnityEngine.Object dragged_object in DragAndDrop.objectReferences)
            {
                //SceneAsset scene = dragged_object as SceneAsset;
                //String path = AssetDatabase.GetAssetPath(scene.GetInstanceID());
                path = AssetDatabase.GetAssetPath(dragged_object);
            }

        }
    }


}
