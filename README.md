Unity Multipart Fileupload
===========

Did this to conform with Spring multipart file receive.

Based on the following answers:
https://answers.unity.com/questions/1354080/unitywebrequestpost-and-multipartform-data-not-for.html
https://answers.unity.com/questions/48686/uploading-photo-and-video-on-a-web-server.html

And based on this documentation snippet:
https://docs.unity3d.com/Manual/UnityWebRequest-SendingForm.html

From Unity:
Sending a form to an HTTP server (POST)
There are two primary functions for sending data to a server formatted as a HTML form. If you are migrating over from the WWW system, see Using WWWForm, below.